import React, { Fragment, useState } from 'react';
import {Link} from 'react-router-dom';
import SearchBox from './SearchBox'
import Pagination from './Pagination'


const CharactersList = ({characters}) => {
    const [searchField, saveSearchField] = useState([]);
    const [currentPage, setCurrentPage] = useState(1);
    const [charactersPerPage] = useState(10);

    const filteredCharacters = characters.filter(character => (character.character.name.toString().toLowerCase().includes(searchField.toString().toLowerCase())))

    const indexOfLastCharacter = currentPage * charactersPerPage;
    const indecOfFirstCharacter = indexOfLastCharacter - charactersPerPage;
    const currentCharacters = filteredCharacters.slice(indecOfFirstCharacter,indexOfLastCharacter);

    const paginate = (pageNumber) => setCurrentPage(pageNumber)

    return ( 
    <Fragment>
        <h1 className="my-5">GOT Character List</h1>

        <div className="container mt-5 py-5">
            <div className="row">
                <div className="col-12 mb-5 d-flex justify-content-center">
                    <SearchBox 
                    placeholder="Enter name"
                    handleChange={(e) => saveSearchField(e.target.value)}
                    />
                </div>
                <Pagination charactersPerPage={charactersPerPage} totalCharacters={filteredCharacters.length} paginate={paginate}/>
                <div className="col-md-8 mx-auto">
                    <div className="list-group">
                        {currentCharacters.map(character => (
                            <Link to={`/characters/${character._id}`} key={character._id} className="p-5 list-group-item list-group-item-action flex-column align-items-start">
                                <div className="d-flex w-100 justify-content-between mb-4">
                                    <h3  className="mb-3">{character.character.name}</h3>
                                    <small class="flecha-alta">
                                        {character.character.house}
                                    </small>
                                </div>
                            </Link>
                        ))}
                    </div>
                </div>
            </div>
        </div>
    </Fragment>
    
    )
}

export default CharactersList;