import React, { Fragment} from 'react';
import { Link, withRouter } from 'react-router-dom';


const Character = (props) => {

    if(!props.character){
        props.history.push("/characters");
        return null;
    }

    const { character: {name, house,gender, slug, pagerank, image, titles} } = props;

    return ( 
        <Fragment>
            <h1 className="my-5">Character : {name}</h1>

            <div className="container mt-5 py5">
                <div className="row">
                    <div className="col-12 mh-5 d-flex justify-content-center">
                        <Link to={"/characters"} className="btn btn-success text-uppercase py-2 px-5 font-weight-cold">Volver</Link>
                    </div>
                </div>
            </div>
            <div className="col-md-8 mx-auto">
                <div className="list-group">
                    <div className="p-5 list-group-item list-group-item-action flex-column aling-items-center">
                        <img src={image} alt="pic" />
                        <div className=" d-flex w-100 justify-content-between mb-4">
                            <h3 className="mb-3">{name}</h3>
                            <small className="fecha-alta">
                                House:{house}
                            </small>
                        </div>
                        <p className="mb-0">
                            Sex: {gender}
                        </p>
                        <p className="mb-0">
                            Slug: {slug}
                        </p>
                        <p className="mb-0">
                            Rank: {pagerank.rank}
                        </p>
                        <p className="mb-0">
                            Titles: {titles}
                        </p>
                        <p className="mb-0">
                            Books: "Grabed the api from the show not the general, my bad"-Gustavo Rivera
                        </p>
                    </div>
                </div>
            </div>
        </Fragment>
        )
}

export default withRouter(Character);