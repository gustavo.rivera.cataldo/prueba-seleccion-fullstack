import React, { Fragment} from 'react';
import {Link} from 'react-router-dom';

const Home = () => {
    return ( 
        <Fragment>
        <h1 className="my-5">HOUSE OF GOT</h1>

        <div className="container mt-5 py5">
            <div className="row">
                <div className="col-12 mh-5 d-flex justify-content-center">
                    <Link to={"/characters"} className="btn btn-success text-uppercase py-2 px-5 font-weight-cold">Lista</Link>
                </div>
            </div>
        </div>
        </Fragment>
        )
}

export default Home;