import React, { useEffect, useState} from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import Home from './components/Home';
import CharactersList from './components/CharactersList';
import Character from './components/Character';
import clienteAxios from './config/axios';

function App() {
  const [characters, saveCharacters] = useState([]);
  useEffect( () => {
      const consultarAPI = async () => {
          clienteAxios.get('/characters')
          .then((res)=>{
              console.log(res.data);
              saveCharacters(res.data);
          })
          .catch(()=>{console.log("error")})
      }
      consultarAPI();
   }, [])
  return (
      <Router>
          <Switch>
          <Route
              exact
              path="/"
              component={Home}
              />
            <Route
              exact
              path="/characters"
              component={() => <CharactersList characters={characters} />}
              />
             <Route
              exact
              path="/characters/:id"
              render={(props) =>{
                const character = characters.filter( character => character._id === props.match.params.id)
                console.log(props);

                return(
                  <Character
                    character={character[0].character}
                  />
                )
              }}
              />
          </Switch>
      </Router>
  );
}

export default App;
