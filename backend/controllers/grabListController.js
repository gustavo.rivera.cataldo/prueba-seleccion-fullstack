const axios = require('axios');
let characterList = require("../models/characters");

//CyberMonday i hate u

exports.grabCharacterList = (req, res, next) => {
    characterList.collection.drop();
    axios.get('https://api.got.show/api/show/characters')
    .then(function (response) {
        let count = 0;
        const data = response.data;
        data.forEach(async character => {
          try{
            const newCharacterList = await characterList({
              character
            })
            newCharacterList.save();
            count ++
            console.log("saved character number " + count);
          }catch(error){
            console.log(error)
          }
        });

    })
    .catch(function (error) {
      console.log(error);
    })
    res.send(200)
}

exports.getList = async (req, res, next) =>{
    try{
        const lista = await characterList.find({});
        res.json(lista)
    }catch{
      console.log(error);
      next();
    }
}

exports.getCharacter = async (req, res, next) =>{
    try{
        const character = await characterList.findById(req.params.id)
        res.json(character);
    }catch(error){
      console.log(error);
      next();
    }
}