const  mongoose  = require('mongoose');
const express = require('express');
const routes = require('./routes');
const bodyParser = require('body-parser');
const cors = require('cors');
const axios = require('axios');
let characterList = require("./models/characters");

const app = express();
app.use (cors());

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost/gameofthrones', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false
}).then(()=>{console.log("Conectado a la Base de Datos")})

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.use('/', routes())

app.listen(4000, () => {
    console.log("Servidor Funcionando")
})

characterList.collection.drop();
axios.get('https://api.got.show/api/show/characters')
.then(function (response) {
    let count = 0;
    const data = response.data;
    data.forEach(async character => {
      try{
        const newCharacterList = await characterList({
          character
        })
        newCharacterList.save();
        count ++
      }catch(error){
        console.log(error)
      }
    });

})