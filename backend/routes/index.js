const express = require('express');
const router = express.Router();
const grabListController = require('../controllers/grabListController');

module.exports = function(){
    // router.get('/', grabListController.grabCharacterList);
    router.get('/characters', grabListController.getList);
    router.get('/characters/:id', grabListController.getCharacter);

    return router;
}