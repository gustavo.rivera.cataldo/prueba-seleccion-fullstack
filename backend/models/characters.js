const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const gotCharactesSchema = new Schema({
    character:{
        type: Object
    }
})

module.exports = mongoose.model('GotCharacter', gotCharactesSchema)